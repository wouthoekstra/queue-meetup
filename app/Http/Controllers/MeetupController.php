<?php

namespace App\Http\Controllers;

use App\Events\MeetupBroadcaster;
use App\Jobs\ExampleJob;
use Illuminate\Http\Request;

class MeetupController extends Controller
{
    public function input($id)
    {
        return view('welcome', compact('id'));
    }

    public function queue(Request $request)
    {
        $id  = $request->id;
        $job = new ExampleJob();

        $job->setId($id);
        $job->setMessage('deploying reversor™');
        $job->setData($request->data);

        dispatch($job);
        event(new MeetupBroadcaster($id,'pushed job to queue'));

        return redirect('/listener/' . $id);
    }

    public function listener($id)
    {
        return view('listener', compact('id'));
    }
}

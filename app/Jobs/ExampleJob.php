<?php

namespace App\Jobs;

use App\Events\MeetupBroadcaster;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ExampleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $id;
    private $data;
    private $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        event(new MeetupBroadcaster($this->getId(), 'input: ' . $this->getData()));

        sleep(4);

        event(new MeetupBroadcaster($this->getId(), $this->getMessage()));

        $manipulation = $this->superComplicatedFunction();

        Log::info(sprintf('Reversed %s into %s', $this->getData(), $manipulation));

        event(new MeetupBroadcaster($this->getId(), $manipulation));
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    private function superComplicatedFunction()
    {
        sleep(4);

        return strrev($this->getData());
    }
}

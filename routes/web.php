<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{id}', ['as' => 'welcome', 'uses' => 'MeetupController@input']);

Route::post('/queue',  ['as' => 'dispatch', 'uses' => 'MeetupController@queue']);

Route::get('listener/{id}', ['as' => 'listener', 'uses' => 'MeetupController@listener']);

Auth::routes();

Route::get('/home', 'HomeController@index');
